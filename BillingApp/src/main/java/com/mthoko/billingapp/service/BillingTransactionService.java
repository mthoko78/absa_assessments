package com.mthoko.billingapp.service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mthoko.billingapp.dao.BillingTransactionRepo;
import com.mthoko.billingapp.entity.BillingSummary;
import com.mthoko.billingapp.entity.BillingTransaction;

@Component
public class BillingTransactionService {

	private final BillingTransactionRepo repo;

	public BillingTransactionService(@Autowired BillingTransactionRepo repo) {
		this.repo = repo;
		if (repo.count() == 0) {
			loadSampleData("sampleTransactions.json");
		}
	}

	public void save(BillingTransaction billingTransaction) {
		repo.save(billingTransaction);
	}

	public List<BillingTransaction> findAll() {
		return repo.findAll();
	}

	private void loadSampleData(String filePath) {
		try {
			InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filePath);
			byte[] bytes = new byte[resourceAsStream.available()];
			resourceAsStream.read(bytes);
			String fileContents = new String(bytes);
			List<BillingTransaction> sampleTransactions = retrieveTransactionsFromJSON(fileContents);
			repo.saveAll(sampleTransactions);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<BillingTransaction> retrieveTransactionsFromJSON(String jsonData) throws IOException {
		List<BillingTransaction> transactions = new ArrayList<>();
		JSONArray array = new JSONArray(jsonData);
		for (int i = 0; i < array.length(); i++) {
			transactions.add(toBillingTransaction(array.getJSONObject(i)));
		}
		return transactions;
	}

	private List<BillingSummary> getBillingSummaries(LocalDate date, String clientSwiftAddress,
			List<BillingTransaction> clientTransactions) {
		List<BillingSummary> billingSummaries = new ArrayList<>();
		Map<String, List<BillingTransaction>> transactionsGroupedBySubService = groupedTransactions(clientTransactions);
		transactionsGroupedBySubService.forEach(generateBillingSummary(date, clientSwiftAddress, billingSummaries));
		return billingSummaries;
	}

	private Map<String, List<BillingTransaction>> groupedTransactions(List<BillingTransaction> clientTransactions) {
		return clientTransactions.stream().collect(Collectors.groupingBy(subServiceGrouping()));
	}

	private Function<BillingTransaction, String> subServiceGrouping() {
		return (t) -> t.getCurrency().contentEquals("ZAR") ? "ZAROUT" : "CCYOUT";
	}

	private BiConsumer<String, List<BillingTransaction>> generateBillingSummary(LocalDate date,
			String clientSwiftAddress, List<BillingSummary> billingSummaries) {
		return (sub, trans) -> {
			BillingSummary sum = new BillingSummary("INTEGRATEDSERVICES", clientSwiftAddress, sub, date, trans.size());
			billingSummaries.add(sum);
		};
	}

	private BillingTransaction toBillingTransaction(JSONObject jsonObject) {
		BillingTransaction transaction = new BillingTransaction();
		transaction.setAmount(jsonObject.getDouble("amount"));
		transaction.setClientSwiftAddress(jsonObject.getString("clientSwiftAddress"));
		transaction.setCurrency(jsonObject.getString("currency"));
		transaction.setDateTimeCreated(LocalDate.parse(jsonObject.getString("dateTimeCreated")));
		transaction.setId(jsonObject.getInt("id"));
		transaction.setMessageStatus(jsonObject.getString("messageStatus"));
		transaction.setTransactionReference(jsonObject.getString("transactionReference"));
		return transaction;
	}

	public List<BillingTransaction> findCompletedTransactions() {
		return repo.findCompletedTransactions();
	}

	public List<BillingTransaction> findCompletedTransactions(LocalDate startDate, LocalDate endDate) {
		return repo.findCompletedTransactions(startDate, endDate);
	}

	public List<BillingSummary> billingSummary(LocalDate startDate, LocalDate endDate) {
		List<BillingTransaction> transactions = findCompletedTransactions(startDate, endDate);
		Map<String, List<BillingTransaction>> groupedTransactions = groupTransactionsBySwiftAddress(transactions);
		List<BillingSummary> billingSummaries = new ArrayList<>();
		groupedTransactions.forEach(addBillingSummaries(startDate, billingSummaries));
		return billingSummaries;
	}

	private Map<String, List<BillingTransaction>> groupTransactionsBySwiftAddress(
			List<BillingTransaction> completedTransactions) {
		return completedTransactions.stream().collect(Collectors.groupingBy((t) -> t.getClientSwiftAddress()));
	}

	private BiConsumer<? super String, ? super List<BillingTransaction>> addBillingSummaries(LocalDate startDate,
			List<BillingSummary> billingSummaries) {
		return (address, transaction) -> billingSummaries.addAll(getBillingSummaries(startDate, address, transaction));
	}

	public void printBillingSummary(List<BillingSummary> billingSummaryList) {
		String lineSeperator = getLineSeperator();
		System.out.println(lineSeperator);
		System.out.println(getTableHeading());
		System.out.println(lineSeperator);
		for (BillingSummary billingSummary : billingSummaryList) {
			System.out.println(getTableRow(billingSummary));
		}
		System.out.println(lineSeperator + "\n\n");
	}

	private String getTableHeading() {
		return String.format("|%-20s|%-15s|%-15s|%-15s|%-25s|", "Service name", "Swift address", "Sub service", "Date",
				"Usage for previous month");
	}

	private String getTableRow(BillingSummary summary) {
		String row = String.format("|%-20s|%-15s|%-15s|%-15s|%-25s|", summary.getServiceName(),
				summary.getClientSwiftAddress(), summary.getSubService(), summary.getDate(),
				summary.getUsageStatsForPreviousMonth());
		return row;
	}

	private String getLineSeperator() {
		return "----------------------" + "---------------------" + "------------------------"
				+ "-----------------------------";
	}

}

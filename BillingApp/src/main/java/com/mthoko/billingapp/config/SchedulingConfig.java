package com.mthoko.billingapp.config;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.mthoko.billingapp.entity.BillingSummary;
import com.mthoko.billingapp.service.BillingSummaryService;
import com.mthoko.billingapp.service.BillingTransactionService;
import com.mthoko.billingapp.service.FileManager;

@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
public class SchedulingConfig {

	@Autowired
	private BillingTransactionService transactionService;
	
	@Autowired
	private BillingSummaryService summaryService;
	
	private final FileManager fileManager = new FileManager();

	private final LocalDate START_DATE = LocalDate.of(2018, 2, 1);

	private final int TOTAL_MONTHS_TO_RUN = 18;

	private int currentMonth = 1;
	private LocalDate currentDate = START_DATE;  

	/*
		run 1st day of every month
		this will wait for the actual month before it actually runs
	 */
	@Scheduled(cron = "0 0 0 1 * ?")
	public void runMonthly() {
		if (currentMonth < TOTAL_MONTHS_TO_RUN) {
			processBillingOnDate(currentDate);
			currentDate = START_DATE.plusMonths(currentMonth);
			currentMonth++;
		}
	}

	/*
		run every second for demonstration purposes only
		each iteration mimics monthly billing
	 */
	@Scheduled(fixedRate = 1000L)
	public void runPerSecond() {
		if (currentMonth < TOTAL_MONTHS_TO_RUN) {
			processBillingOnDate(currentDate);
			currentDate = START_DATE.plusMonths(currentMonth);
			currentMonth++;
		}
	}
	
	public void processBillingOnDate(LocalDate date) {
		LocalDate previousMonth = date.minusMonths(1);
		LocalDate endDate = previousMonth.plusDays(previousMonth.lengthOfMonth() - previousMonth.getDayOfMonth());
		List<BillingSummary> billingSummary = transactionService.billingSummary(previousMonth, endDate);
		summaryService.saveAll(billingSummary);
		String fileContents = summaryService.toText(billingSummary);
		transactionService.printBillingSummary(billingSummary);
		File file = fileManager.writeToFile(fileContents);
		fileManager.uploadFile(file);
	}
}

package com.mthoko.billingapp.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mthoko.billingapp.entity.BillingTransaction;

@Repository
public interface BillingTransactionRepo extends JpaRepository<BillingTransaction, Integer> {

	@Query("from BillingTransaction where id = 1")
	public BillingTransaction findByDate();

	@Query("select t from BillingTransaction t where t.messageStatus = 'Completed'")
	public List<BillingTransaction> findCompletedTransactions();

	@Query("select t from BillingTransaction t where t.messageStatus = 'Completed' and t.dateTimeCreated in (?1, ?2)")
	public List<BillingTransaction> findCompletedTransactions(LocalDate startDate, LocalDate endDate);
}

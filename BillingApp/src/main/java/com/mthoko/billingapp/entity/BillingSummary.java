package com.mthoko.billingapp.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Component
@Entity
public class BillingSummary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String serviceName;
	
	private String clientSwiftAddress;
	
	private String subService;
	
	private LocalDate date;
	
	private int usageStatsForPreviousMonth;

	
	public BillingSummary() {
	}
	
	public BillingSummary(String serviceName, String clientSwiftAddress, String subService, LocalDate date,
			int usageStatsForPreviousMonth) {
		super();
		this.serviceName = serviceName;
		this.clientSwiftAddress = clientSwiftAddress;
		this.subService = subService;
		this.date = date;
		this.usageStatsForPreviousMonth = usageStatsForPreviousMonth;
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getSubService() {
		return subService;
	}

	public void setSubService(String subService) {
		this.subService = subService;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getUsageStatsForPreviousMonth() {
		return usageStatsForPreviousMonth;
	}

	public void setUsageStatsForPreviousMonth(int usageStatsForPreviousMonth) {
		this.usageStatsForPreviousMonth = usageStatsForPreviousMonth;
	}

	@Override
	public String toString() {
		return "BillingSummary [serviceName=" + serviceName + ", clientSwiftAddress=" + clientSwiftAddress
				+ ", subService=" + subService + ", date=" + date + ", usageStatsForPreviousMonth="
				+ usageStatsForPreviousMonth + "]";
	}
	
}

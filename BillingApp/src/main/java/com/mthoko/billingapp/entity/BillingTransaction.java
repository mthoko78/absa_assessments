package com.mthoko.billingapp.entity;


import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Component
@Entity
public class BillingTransaction {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String transactionReference;

	private String clientSwiftAddress;

	private String messageStatus;

	private String currency;

	private Double amount;

	private LocalDate dateTimeCreated;

	public BillingTransaction() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public LocalDate getDateTimeCreated() {
		return dateTimeCreated;
	}

	public void setDateTimeCreated(LocalDate dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}

	@Override
	public String toString() {
		return "BillingTransaction [id=" + id + ", transactionReference=" + transactionReference
				+ ", clientSwiftAddress=" + clientSwiftAddress + ", messageStatus=" + messageStatus + ", currency="
				+ currency + ", amount=" + amount + ", dateTimeCreated=" + dateTimeCreated + "]";
	}

}

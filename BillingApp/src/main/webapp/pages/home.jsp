<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Billing Application</title>
</head>
<body>
	<h1>Welcome</h1>
	<h2>Please select date to view transactions from</h2>
	<form action="transactions" method="post">
		<input type="date" name="date" /> <input type="submit" value="Submit" />
	</form>
	<h2>Click to view all transactions</h2>
	<form action="allTransactions" method="post">
		<input type="submit" value="All transactions" />
	</form>
	<h2>Click to view completed transactions</h2>
	<form action="completedTransactions" method="post">
		<input type="submit" value="Complete transactions" />
	</form>
	<h2>Click to view transaction billings</h2>
	<form action="billingSummary" method="post">
		<input type="date" name="date" /> <input type="submit"
			value="Billing summary" />
	</form>
</body>
</html>
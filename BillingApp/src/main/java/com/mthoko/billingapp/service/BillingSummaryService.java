package com.mthoko.billingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mthoko.billingapp.dao.BillingSummaryRepo;
import com.mthoko.billingapp.entity.BillingSummary;

@Component
public class BillingSummaryService {

	private final BillingSummaryRepo repo;

	public BillingSummaryService(@Autowired BillingSummaryRepo repo) {
		this.repo = repo;
	}

	public void save(BillingSummary billingSummary) {
		repo.save(billingSummary);
	}
	
	public void saveAll(List<BillingSummary> billingSummarList) {
		repo.saveAll(billingSummarList);
		
	}

	public List<BillingSummary> findAll() {
		return repo.findAll();
	}

	public String toText(List<BillingSummary> billingSummaryList) {
		StringBuilder allText = new StringBuilder();
		for (BillingSummary summary : billingSummaryList) {
			String text = String.format("%-18s%-11s%-6s%-8s%-6s", 
					summary.getServiceName(), 
					summary.getClientSwiftAddress(),
					summary.getSubService(),
					summary.getDate(),
					summary.getUsageStatsForPreviousMonth()
					);
			allText.append(text).append("\n");
		}
		return allText.toString();
	}
}

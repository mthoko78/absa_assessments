package com.mthoko.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SequentialAndParallelStreams {

	public static void printNonEmptyStatusesStreams() {
		List<String> statuses = Arrays.asList("Received", "Pending", "","Awaiting",
				"Maturity", "Completed","");
		List<String> statusesUsingParallelStreams = getNonEmptyStringsUsingParallelStreams(statuses);
		List<String> statusesUsingSequentialStreams = getNonEmptyStringsUsingSequentialStreams(statuses);
		System.out.println("Statuses before filtering: " + statuses);
		System.out.println("Statuses filterd using parallel streams: \n" + statusesUsingParallelStreams);
		System.out.println("Statuses filterd using sequential streams: \n" + statusesUsingSequentialStreams);
		
	}
	
	public static List<String> getNonEmptyStringsUsingParallelStreams(List<String> list) {
		List<String> nonEmptyStrings = list.parallelStream().filter(s -> s != null && !s.isEmpty())
				.collect(Collectors.toList());
		return nonEmptyStrings;
	}
	
	public static List<String> getNonEmptyStringsUsingSequentialStreams(List<String> list) {
		List<String> nonEmptyStrings = list.stream().filter(s -> s != null && !s.isEmpty())
				.collect(Collectors.toList());
		return nonEmptyStrings;
	}

}

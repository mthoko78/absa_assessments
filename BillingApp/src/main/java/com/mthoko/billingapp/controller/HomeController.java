
package com.mthoko.billingapp.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mthoko.billingapp.entity.BillingSummary;
import com.mthoko.billingapp.entity.BillingTransaction;
import com.mthoko.billingapp.service.BillingTransactionService;

@Controller
public class HomeController {

	private final BillingTransactionService transactionService;

	@Autowired
	public HomeController(BillingTransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@RequestMapping("home")
	public ModelAndView home() {
		ModelAndView mv = new ModelAndView();
		return mv;
	}

	@RequestMapping("/transactions")
	@ResponseBody
	public String getTransactionsByDate(@DateTimeFormat(iso = ISO.DATE) @RequestParam("date") LocalDate date,
			@Autowired BillingTransaction transaction) {
		transaction.setDateTimeCreated(date);
		transactionService.save(transaction);
		return "WELCOME. . . your transaction is: " + transaction;
	}

	@RequestMapping("/allTransactions")
	@ResponseBody
	public List<BillingTransaction> transactions() {
		return transactionService.findAll();
	}

	@RequestMapping("completedTransactions")
	@ResponseBody
	public List<BillingTransaction> completedTransactions() {
		List<BillingTransaction> completedTransactions = transactionService.findCompletedTransactions();
		return completedTransactions;
	}

	@RequestMapping("billingSummary")
	@ResponseBody
	public List<BillingSummary> billingSummary(@DateTimeFormat(iso = ISO.DATE) @RequestParam("date") LocalDate date) {
		LocalDate startDate = date.minusMonths(1);
		startDate = startDate.minusDays(startDate.getDayOfMonth() - 1);
		LocalDate endDate = LocalDate.of(startDate.getYear(), startDate.getMonthValue(), startDate.lengthOfMonth());
		return transactionService.billingSummary(startDate, endDate);
	}
	
}

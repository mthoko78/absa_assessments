package com.mthoko.billingapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mthoko.billingapp.entity.BillingSummary;;

@Repository
public interface BillingSummaryRepo extends JpaRepository<BillingSummary, Integer>{

}
